package com.lenovo.manufacture;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.beans.AddUserMaterial;
import com.lenovo.manufacture.beans.AllUserProductionLine;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SixActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SixActivity";
    private ImageView img_six_carone;
    private Button btn_six_carone;
    private ImageView img_six_cartwo;
    private Button btn_six_cartwo;
    private ImageView img_six_carthree;
    private Button btn_six_carthree;
    private ImageView img_six_carfour;
    private Button btn_six_carfour;
    private boolean[] flags = {false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six);

        loadData();
    }

    private void loadData() {
        new Thread() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/UserProductionLine/getAll")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string = response.body().string();
                    Log.d(TAG, "run: " + string);
                    Gson gson = new Gson();
                    AllUserProductionLine allUserProductionLine = gson.fromJson(string, AllUserProductionLine.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initView(allUserProductionLine.getData());
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void initView(List<AllUserProductionLine.DataBean> data) {
        img_six_carone = (ImageView) findViewById(R.id.img_six_carone);
        btn_six_carone = (Button) findViewById(R.id.btn_six_carone);
        img_six_cartwo = (ImageView) findViewById(R.id.img_six_cartwo);
        btn_six_cartwo = (Button) findViewById(R.id.btn_six_cartwo);
        img_six_carthree = (ImageView) findViewById(R.id.img_six_carthree);
        btn_six_carthree = (Button) findViewById(R.id.btn_six_carthree);
        img_six_carfour = (ImageView) findViewById(R.id.img_six_carfour);
        btn_six_carfour = (Button) findViewById(R.id.btn_six_carfour);

        btn_six_carone.setOnClickListener(this);
        btn_six_cartwo.setOnClickListener(this);
        btn_six_carthree.setOnClickListener(this);
        btn_six_carfour.setOnClickListener(this);

        for (AllUserProductionLine.DataBean dataBean : data) {
            switch (dataBean.getPosition()) {
                case 0:
                    img_six_carone.setImageDrawable(getDrawable(R.drawable.six_one));
                    flags[0] = true;
                    break;
                case 1:
                    img_six_cartwo.setImageDrawable(getDrawable(R.drawable.six_one));
                    flags[1] = true;
                    break;
                case 2:
                    img_six_carthree.setImageDrawable(getDrawable(R.drawable.six_one));
                    flags[2] = true;
                    break;
                case 3:
                    img_six_carfour.setImageDrawable(getDrawable(R.drawable.six_one));
                    flags[3] = true;
                    break;
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_six_carone:
                if (flags[0]) {
                    getItem();
                } else {
                    Toast.makeText(getApplicationContext(), "该车间没有生产线，无法补货", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_six_cartwo:
                if (flags[1]) {
                    getItem();
                } else {
                    Toast.makeText(getApplicationContext(), "该车间没有生产线，无法补货", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_six_carthree:
                if (flags[2]) {
                    getItem();
                } else {
                    Toast.makeText(getApplicationContext(), "该车间没有生产线，无法补货", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_six_carfour:
                if (flags[3]) {
                    getItem();
                } else {
                    Toast.makeText(getApplicationContext(), "该车间没有生产线，无法补货", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void getItem() {
        new Thread() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/Interface/index/addUserMaterial?userLineId=2431")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string = response.body().string();
                    Log.d(TAG, "run: string:" + string);
                    Gson gson = new Gson();
                    AddUserMaterial addUserMaterial = gson.fromJson(string, AddUserMaterial.class);
//                    String message = response.message();
//                    Log.d(TAG, "run: message:" + message);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), addUserMaterial.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }
}
