package com.lenovo.manufacture;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.beans.UpdateLightSwitch;
import com.lenovo.manufacture.beans.UserWorkEnvironmentInfo;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OneActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "OneActivity";
    private ImageView img_light;
    private Button btn_open;
    private Button btn_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);


        loadData();

    }

    private void loadData() {
        new Thread() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/UserWorkEnvironmental/getInfo?id=1")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String msg = response.body().string();
                    Log.d(TAG, "run: loadData" + msg);
                    Gson gson = new Gson();
                    UserWorkEnvironmentInfo userWorkEnvironmentInfo = gson.fromJson(msg, UserWorkEnvironmentInfo.class);
                    UserWorkEnvironmentInfo.DataBean dataBean = userWorkEnvironmentInfo.getData().get(0);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initView(dataBean);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    private void initView(UserWorkEnvironmentInfo.DataBean dataBean) {
        img_light = (ImageView) findViewById(R.id.img_light);
        img_light.setOnClickListener(this);
        btn_open = (Button) findViewById(R.id.btn_open);
        btn_open.setOnClickListener(this);
        btn_close = (Button) findViewById(R.id.btn_close);
        btn_close.setOnClickListener(this);

        int lightSwitch = dataBean.getLightSwitch();
        if (lightSwitch == 0) {
            //关闭
            img_light.setImageDrawable(getDrawable(R.drawable.gongchang_02));
        } else {
            //开启
            img_light.setImageDrawable(getDrawable(R.drawable.gongchang_01));
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_open:
                sendLightSwitch(1);
                //开启
                img_light.setImageDrawable(getDrawable(R.drawable.gongchang_01));
                Toast.makeText(getApplicationContext(),"开启灯光",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_close:
                sendLightSwitch(0);
                //关闭
                img_light.setImageDrawable(getDrawable(R.drawable.gongchang_02));
                Toast.makeText(getApplicationContext(),"关闭灯光",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void sendLightSwitch(int openLight) {
        new Thread() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/UserWorkEnvironmental/updateLightSwitch?id=1&lightSwitch=" + openLight)
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    Log.d(TAG, "run: sendLightSwitch" + response.body().string());
//            Gson gson = new Gson();
//            UpdateLightSwitch updateLightSwitch = gson.fromJson(response.body().string(), UpdateLightSwitch.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

}