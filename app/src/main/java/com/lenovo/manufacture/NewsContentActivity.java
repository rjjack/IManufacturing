package com.lenovo.manufacture;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.model.NewsInfo;

import static com.lenovo.manufacture.NewsActivity.IP;

public class NewsContentActivity extends AppCompatActivity {
    private NewsInfo.DataBean data;
    private TextView tv_t;
    private VideoView vv;
    private TextView tv_c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_content);
        getData();
        initView();
        play();

    }

    private void play() {
        if (data != null && data.getVideo() != null) {
            vv.setVideoURI(Uri.parse(IP + data.getVideo()));
            if (vv.isPlaying()) {
                vv.pause();
            } else {
                vv.start();
            }

        } else {
            Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
        }
    }

    private void getData() {
        String stringExtra = getIntent().getStringExtra(NewsActivity.KEY);
        if (stringExtra.isEmpty()) {
            Toast.makeText(this, "错误", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            try {
                data = new Gson().fromJson(stringExtra, NewsInfo.DataBean.class);
            } catch (Exception e) {
                Log.e("NewsContentActivity", e.getLocalizedMessage(), e);
            }
        }
    }

    private void initView() {
        tv_t = (TextView) findViewById(R.id.tv_t);
        vv = (VideoView) findViewById(R.id.vv);
        tv_c = (TextView) findViewById(R.id.tv_c);
        vv.setOnClickListener((v -> play()));
        tv_t.setText(data == null ? "null" : data.getInformationName());
        tv_c.setText(data == null ? "null" : data.getWords());
    }
}
