package com.lenovo.manufacture;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.lenovo.manufacture.model.NewsInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NewsActivity extends AppCompatActivity {
    private List<NewsInfo.DataBean> newsInfoList = new ArrayList<>();
    private ArrayAdapter<NewsInfo.DataBean> adapter;
    private ListView lv_news;
    public static final String KEY = "NewsInfo";
public static final String IP ="http://192.168.1.103";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        initView();
        getData();

    }

    void getData() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(IP+":8085/dataInterface/Information/getAll")
                .method("POST", body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(() -> {
                    Toast.makeText(NewsActivity.this, "失败", Toast.LENGTH_SHORT).show();
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    NewsInfo newsInfo = gson.fromJson(response.body().string(), NewsInfo.class);
                    newsInfoList.clear();
                    newsInfoList.addAll(newsInfo.getData());
                    runOnUiThread(() -> {
                        Toast.makeText(NewsActivity.this, "成功", Toast.LENGTH_SHORT).show();
                        adapter.notifyDataSetChanged();
                    });
                } else {
                    runOnUiThread(() -> {
                        Toast.makeText(NewsActivity.this, "失败" + response.code(), Toast.LENGTH_SHORT).show();
                    });
                }

            }
        });
    }

    private void initView() {
        lv_news = (ListView) findViewById(R.id.lv_news);
        int item_news = R.layout.item_news;
        adapter = new ArrayAdapter<NewsInfo.DataBean>(this, item_news, newsInfoList) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                convertView = LayoutInflater.from(getContext()).inflate(item_news, parent, false);
                ViewHolder viewHolder = new ViewHolder(convertView);
                NewsInfo.DataBean item = getItem(position);
                viewHolder.tv_c.setText(item.getWords());
                viewHolder.tv_t.setText(item.getInformationName());

                String icon = item.getIcon();
                if (icon==null||icon.isEmpty()||icon.contains("null")) {
                    viewHolder.iv.setVisibility(View.GONE);
                }else {
                    Glide.with(getContext())
                            .load(IP+icon)
                            .placeholder(R.drawable.ic_refresh)
                            .error(R.drawable.ic_error)
                            .override(180, 100)
                            .into(viewHolder.iv);
                }

                return convertView;
            }
        };
        lv_news.setAdapter(adapter);
        lv_news.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(this, NewsContentActivity.class);
            String s = new Gson().toJson(newsInfoList.get(position));
            intent.putExtra(KEY, s);
            startActivity(intent);
        });
    }

    static class ViewHolder {
        public View rootView;
        public TextView tv_t;
        public ImageView iv;
        public TextView tv_c;

        public ViewHolder(View rootView) {
            this.rootView = rootView;
            this.tv_t = (TextView) rootView.findViewById(R.id.tv_t);
            this.iv = (ImageView) rootView.findViewById(R.id.iv);
            this.tv_c = (TextView) rootView.findViewById(R.id.tv_c);
        }
    }
}
