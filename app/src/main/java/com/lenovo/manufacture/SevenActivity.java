package com.lenovo.manufacture;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.beans.UserSellInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SevenActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SevenActivity";
    private TextView tv_six_total_price;
    private TextView tv_six_dazhong_price;
    private TextView tv_six_jipu_price;
    private TextView tv_six_bluecar_price;
    private WebView seven_wv;
    private ListView seven_lv;
    private int oneTotal;
    private int twoTotal;
    private int threeTotal;
    private int totalMoney;
    private MyAdapter myAdapter;
    private TextView tv_price;
    private TextView tv_sell_time;
    private TextView tv_sell_count;
    private List<UserSellInfo.DataBean> data;
    private boolean priceFlag = false;
    private boolean sellTimeFlag = false;
    private boolean sellCountFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seven);

        loadData();

    }

    private void loadData() {
        new Thread() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/Interface/index/userSellInfoTEditer")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string = response.body().string();
                    Gson gson = new Gson();
                    UserSellInfo userSellInfo = gson.fromJson(string, UserSellInfo.class);
                    Log.d(TAG, "run: string:" + string);
                    data = userSellInfo.getData();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initView();
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    private void initView() {
        //1 轿车 2 mpv 3 suv
        tv_six_total_price = (TextView) findViewById(R.id.tv_six_total_price);
        tv_six_dazhong_price = (TextView) findViewById(R.id.tv_six_dazhong_price);
        tv_six_jipu_price = (TextView) findViewById(R.id.tv_six_jipu_price);
        tv_six_bluecar_price = (TextView) findViewById(R.id.tv_six_bluecar_price);
        seven_wv = (WebView) findViewById(R.id.seven_wv);
        seven_lv = (ListView) findViewById(R.id.seven_lv);

        JSONArray xAxis = new JSONArray();
        JSONArray values = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd");
        for (UserSellInfo.DataBean dataBean : data) {
            xAxis.put(simpleDateFormat.format(dataBean.getTime()));
            int price = dataBean.getPrice() * dataBean.getNum();
            values.put(price);
            switch (dataBean.getCarTypeId()) {
                case 1:
                    oneTotal += price;
                    break;
                case 2:
                    twoTotal += price;
                    break;
                case 3:
                    threeTotal += price;
                    break;
            }
        }
        totalMoney = oneTotal + twoTotal + threeTotal;
        tv_six_total_price.setText(totalMoney + "");
        tv_six_dazhong_price.setText(oneTotal + "");
        tv_six_jipu_price.setText(twoTotal + "");
        tv_six_bluecar_price.setText(threeTotal + "");

        myAdapter = new MyAdapter(getApplicationContext(), R.layout.item_seven_lv, data);
        seven_lv.setAdapter(myAdapter);

        seven_wv.getSettings().setJavaScriptEnabled(true);
        seven_wv.loadUrl("file:///android_asset/echart.html");
        seven_wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                try {
                    jsonObject.put("xAxis", xAxis);
                    jsonObject.put("value", values);
                    Log.d(TAG, "onPageFinished: jsonObject:" + jsonObject.toString());
                    seven_wv.loadUrl("javascript:WriteData('" + jsonObject + "')");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });

        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_price.setOnClickListener(this);
        tv_sell_time = (TextView) findViewById(R.id.tv_sell_time);
        tv_sell_time.setOnClickListener(this);
        tv_sell_count = (TextView) findViewById(R.id.tv_sell_count);
        tv_sell_count.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_price:
                if (priceFlag) {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o1.getPrice() - o2.getPrice();
                        }
                    });
                    tv_price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0002), null);
                } else {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o2.getPrice() - o1.getPrice();
                        }
                    });
                    tv_price.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0004), null);
                }
                priceFlag = !priceFlag;
                myAdapter.notifyDataSetChanged();
                break;
            case R.id.tv_sell_time:
                if (sellTimeFlag) {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o1.getTime() - o2.getTime();
                        }
                    });
                    tv_sell_time.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0004), null);
                } else {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o2.getTime() - o1.getTime();
                        }
                    });
                    tv_sell_time.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0002), null);
                }
                sellTimeFlag = !sellTimeFlag;
                myAdapter.notifyDataSetChanged();
                break;
            case R.id.tv_sell_count:
                if (sellCountFlag) {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o1.getNum() - o2.getNum();
                        }
                    });
                    tv_sell_count.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0002), null);
                } else {
                    Collections.sort(data, new Comparator<UserSellInfo.DataBean>() {
                        @Override
                        public int compare(UserSellInfo.DataBean o1, UserSellInfo.DataBean o2) {
                            return o2.getNum() - o1.getNum();
                        }
                    });
                    tv_sell_count.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0004), null);
                }
                sellCountFlag = !sellCountFlag;
                myAdapter.notifyDataSetChanged();
                break;
        }
    }

    class MyAdapter extends ArrayAdapter {
        List<UserSellInfo.DataBean> data;

        public MyAdapter(@NonNull Context context, int resource, List<UserSellInfo.DataBean> data) {
            super(context, resource);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = View.inflate(getApplicationContext(), R.layout.item_seven_lv, null);
            } else {
                view = convertView;
            }
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.tv_item_seven_lv_name.setText(data.get(position).getCarTypeName());
            viewHolder.tv_item_seven_lv_price.setText(data.get(position).getPrice() + "");
            viewHolder.tv_item_seven_lv_number.setText(data.get(position).getNum() + "");

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-hh-mm");
            viewHolder.tv_item_seven_lv_time.setText(simpleDateFormat.format(data.get(position).getTime()));

            return view;
        }

        class ViewHolder {
            public View rootView;
            public TextView tv_item_seven_lv_name;
            public TextView tv_item_seven_lv_price;
            public TextView tv_item_seven_lv_time;
            public TextView tv_item_seven_lv_number;

            public ViewHolder(View rootView) {
                this.rootView = rootView;
                this.tv_item_seven_lv_name = (TextView) rootView.findViewById(R.id.tv_item_seven_lv_name);
                this.tv_item_seven_lv_price = (TextView) rootView.findViewById(R.id.tv_item_seven_lv_price);
                this.tv_item_seven_lv_time = (TextView) rootView.findViewById(R.id.tv_item_seven_lv_time);
                this.tv_item_seven_lv_number = (TextView) rootView.findViewById(R.id.tv_item_seven_lv_number);
            }

        }
    }

}
