package com.lenovo.manufacture.model;

import java.util.List;

public class UserQuestionCar {

    /**
     * status : 200
     * message : SUCCESS
     * data : [{"id":745,"userWorkId":1,"carId":1,"userProductionLineId":2432},{"id":744,"userWorkId":1,"carId":2,"userProductionLineId":2433},{"id":743,"userWorkId":1,"carId":1,"userProductionLineId":2432},{"id":746,"userWorkId":1,"carId":1,"userProductionLineId":2432},{"id":747,"userWorkId":1,"carId":1,"userProductionLineId":2432}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 745
         * userWorkId : 1
         * carId : 1
         * userProductionLineId : 2432
         */

        private int id;
        private int userWorkId;
        private int carId;
        private int userProductionLineId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserWorkId() {
            return userWorkId;
        }

        public void setUserWorkId(int userWorkId) {
            this.userWorkId = userWorkId;
        }

        public int getCarId() {
            return carId;
        }

        public void setCarId(int carId) {
            this.carId = carId;
        }

        public int getUserProductionLineId() {
            return userProductionLineId;
        }

        public void setUserProductionLineId(int userProductionLineId) {
            this.userProductionLineId = userProductionLineId;
        }
    }
}
