package com.lenovo.manufacture.model;

import java.util.List;

public class UserPlStepInfo {

    /**
     * status : 200
     * message : SUCCESS
     * data : [{"id":"20147","plStepName":"MPV车型生产环节19","stageId":23,"power":100,"costTime":"0","step":19,"consume":30}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 20147
         * plStepName : MPV车型生产环节19
         * stageId : 23
         * power : 100
         * costTime : 0
         * step : 19
         * consume : 30
         */

        private String id;
        private String plStepName;
        private int stageId;
        private int power;
        private String costTime;
        private int step;
        private int consume;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPlStepName() {
            return plStepName;
        }

        public void setPlStepName(String plStepName) {
            this.plStepName = plStepName;
        }

        public int getStageId() {
            return stageId;
        }

        public void setStageId(int stageId) {
            this.stageId = stageId;
        }

        public int getPower() {
            return power;
        }

        public void setPower(int power) {
            this.power = power;
        }

        public String getCostTime() {
            return costTime;
        }

        public void setCostTime(String costTime) {
            this.costTime = costTime;
        }

        public int getStep() {
            return step;
        }

        public void setStep(int step) {
            this.step = step;
        }

        public int getConsume() {
            return consume;
        }

        public void setConsume(int consume) {
            this.consume = consume;
        }
    }
}
