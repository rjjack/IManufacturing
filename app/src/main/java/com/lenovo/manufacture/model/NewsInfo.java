package com.lenovo.manufacture.model;

import java.util.List;

public class NewsInfo {

    /**
     * status : 200
     * message : SUCCESS
     * data : [{"id":20,"informationName":"19款丰田霸道2700特惠原装进口环保公开","time":1571972940,"words":"19款丰田霸道2700是丰田旗下的一款大型SUV，丰田霸道凭仗其良好的越野机能和优良的外观计划和其超高的性价比为其在竞争如斯剧烈的越野车市场夺得冠军，车型如同名字一样，非常的霸道。丰田霸道2700中东版纯正越野利器换了一副全新的面孔重新示人。位于保险杠两侧的圆形雾灯更加的简洁、直观，拥有那仿佛钢铁侠一样的夺人的眼神。全身硬朗的棱线设计更是振奋人心，体现对完美的追求。","video":"/factory_uploads/2019/10/25/20191025110933256.mp4","icon":"/factory_uploads/2019/10/25/20191025110933363.jpg","status":0},{"id":21,"informationName":"2019款三菱帕杰罗v97 金标顶配中东版现车","time":1571877000,"words":"三菱帕杰罗V97中东版3.8L排量，进口3菱帕杰罗V97的越野性能，属于实用派一类。没有过多的噱头，在恶劣条件下的优良的可靠性能带你摆脱任何困境。三菱帕杰罗V97搭载V63.8L发动机 动力方面明显提升 而且在40万之内硬派越野车型里面 别无对手.传统硬派越野车的前脸造型设计，同样出现在了它的前部，3菱车标是身份的象征。V624气门MIVEC 动力优化系统，处处都体现出它的力量与科技，凝视的双眼仿佛一直都在注视着前方的艰难险阻。激情、运动、宽适\u2026\u2026这就是在汽车领域内让酷爱玩车一族羡慕的SUV车型。沉稳越野外观加上超选四驱系统 绝对给力.现车惠降，车身颜色齐全，三菱帕杰罗V97现车充足 喜欢的朋友买车不用愁 要是论40万之内的硬派越野车 那么非帕杰罗不可.相信专业的力量。","video":"/factory_uploads/2019/10/25/20191025111034571.mp4","icon":"/factory_uploads/2019/10/25/20191025111034137.jpg","status":0},{"id":22,"informationName":"补贴后售10.38-11.98万元比亚迪e3正式上市","time":1572030600,"words":"10月24日，比亚迪旗下全新紧凑型车\u2014\u2014比亚迪e3正式上市。新车共推出3款车型，补贴后售价区间为10.38-11.98万元，其可看做是比亚迪e2的三厢版车型。 外观方面，新车依旧采用了\u201cDragon Face\u201d设计元素，前脸六边形进气格栅内部采用了点阵式设计，配合两侧锐利的大灯组以及下方两侧L型进气孔造型，辨识度非常高。车身侧面，凌厉的腰线配合红色刹车卡钳，营造出不错的运动氛围。车尾部，尾灯组采用了贯穿式设计。","video":"/factory_uploads/2019/10/25/20191025111252135.mp4","icon":"/factory_uploads/2019/10/25/20191025111252179.jpg","status":0},{"id":23,"informationName":"比亚迪e3补贴后10.38万元起售！两种续航里程！","time":1572442200,"words":"从现如今看来，发展新能源车是汽车行业发展的大趋势，所以想要在未来站得住脚，布局新能源车领域至关重要。在国内新能源车市场混得风生水起的比亚迪显然是有这种觉悟的，这不，比亚迪e2才上市一个多月，比亚迪就又带来了它的\u201c亲兄弟\u201d\u2014\u2014比亚迪e3。比亚迪e3共推出3款车型，综合补贴后售价为10.38-11.98万元，将归属比亚迪e网进行销售。","video":"/factory_uploads/2019/10/25/20191025111416488.mp4","icon":"/factory_uploads/2019/10/25/20191025111416184.jpg","status":1},{"id":24,"informationName":"第8代高尔夫正式发布，有哪些改变？","time":1571973240,"words":"今天整个汽车圈被刷屏了，原因很简单，就是第8代高尔夫正式发布了官方图片。高尔夫是大众汽车最畅销的车型。并且在他的版本上还开发出了纯电动的车型，以及旅行版，高性能版GTI，在全世界拥有非常多的粉丝。第8代车型在曝光之前，网络上就已经在疯传他的设计图，而今天正式曝光之后，其实与之前的设计图相差并不是非常大，全新的造型设计增加了不少棱角，让前脸看上去更加的犀利，更加的霸气和运动。","video":"/factory_uploads/2019/10/25/20191025111525830.mp4","icon":"/factory_uploads/2019/10/25/20191025111525713.jpg","status":1},{"id":25,"informationName":"普拉多兄弟 比帕杰罗硬派 4.0L+全时四驱","time":1572484200,"words":"丰田普拉多是越野实力派的代表，被国内消费者誉为\"进藏神器\"。这款车型在国内市场一直很受欢迎，质量稳定性，和耐用性被广大自驾游车主作为首选车型。其实，对于喜欢硬派SUV的消费者来说，还有一款SUV值得重点关注，它跟普拉多是兄弟车型，但在外观上，它比帕杰罗更为硬派。它就是丰田旗下的4Runner，这款车型在国内市场称为\"超霸\u201d，越野车中的\u201c纯爷们\u201d。","video":"/factory_uploads/2019/10/25/20191025111609897.mp4","icon":"/factory_uploads/2019/10/25/20191025111609961.jpg","status":1},{"id":26,"informationName":"新车实拍：别克7座中大型SUV昂科旗首次亮相","time":1571903400,"words":"2019年10月22日，上汽通用汽车在北京举办品鉴会，首次公开展示了别克品牌7座中大型SUV\u2014\u2014昂科旗，并邀请泛亚汽车中心的专家，为来宾介绍了该车设计理念。据了解，昂科旗有望在今年年底之前上市销售。　别克品牌的SUV家族，由昂科拉（小型）、昂科拉GX（紧凑型）和昂科威（中型）构成。更高一级的中大型SUV昂科雷，始终以进口车的形式销售。目前，中大型SUV在我国处于高高在上的局面，比如奥迪Q7、宝马X5与奔驰GLE，价格高达六七十万元。大众推出的途昂与马自达推出的CX-8，首次把售价拉进30万元以内，但后者并未获得市场认可。在这种情况下，昂科旗的诞生，不仅在于别克SUV家族品种从此齐全，更是令消费者从此拥有更多选择。","video":"/factory_uploads/2019/10/25/20191025111709998.mp4","icon":"/factory_uploads/2019/10/25/20191025111709671.jpg","status":2},{"id":27,"informationName":"极星2（Polestar2）首发版售价公布 售价41.8万元","time":1572167700,"words":"今日，Polestar中国正式宣布品牌旗下首款纯电动车型极星2（Polestar 2）首发版最终统一零售价格为人民币418,000元，该价格将于2019年10月22日生效，已经支付订金的客户在最终交付车辆时将会按照该最终零售价执行。此次极星2在中国地区的价格调整是基于极星全球的定价策略协同联动统一进行调整后的结果。最终统一零售价的公布无疑将使极星2在中国市场更具竞争力","video":"/factory_uploads/2019/10/25/20191025111757257.mp4","icon":"/factory_uploads/2019/10/25/20191025111757919.jpg","status":2},{"id":28,"informationName":"在滇西高原感受东南DX5 颜值品质驾控\u201c三高\u201d","time":1572223500,"words":"东南汽车在SUV方面，已有DX3与DX7，前不久，又推出DX5。这是一款小型SUV。以级别而论，与DX3相同。那么，厂家为何推出2款同级车？东南DX5的主题是什么？带着这个问题，我在滇西北高原，实地感受了这款新车。对于东南汽车，我的记忆还停留在菱帅、蓝瑟等车型。其中最深的印象是动力与操控非常给力，外观与内饰普普通通。这种特色在10多年前没毛病，但在今天就悬了。如今许多人选车时，把相貌放在首位。第一眼没看上的话，后面\u2026\u2026根本没后面了。就此打住。","video":"/factory_uploads/2019/10/25/20191025111934576.mp4","icon":"/factory_uploads/2019/10/25/20191025111934935.jpg","status":2},{"id":29,"informationName":"本田插电混动车型即将进入我国 比纯电更具优势","time":1572146700,"words":"本田混合动力技术当中的i-MMD，目前在我国，已经应用于雅阁、思威、奥德赛、艾力绅等车型中。在此基础上，本田公司研发的插电式混动系统\u2014\u2014\u201cSPORT HYBRID e＋\u201d，将于2020年进入我国。具体车型虽然尚未公布，但它能在日常行驶中基本实现纯电动化，享受新能源政策的同时，拥有与燃油车相同的续航里程，所以说，这是一项比纯电更有现实意义的新能源技术。","video":"/factory_uploads/2019/10/25/20191025112033972.mp4","icon":"/factory_uploads/2019/10/25/20191025112033904.jpg","status":3},{"id":30,"informationName":"实地体验全新别克威朗 从里到外 焕然一新","time":1572166500,"words":"2019年9月4日，改款之后的别克威朗上市。在此之前，我曾在厂家举办的设计说明会上，观赏过这款车。这次有缘来到浙江奉化溪口，在青山秀水之间，亲身感受了一下这款刚刚上市的新车。与之前车型相比，新车在外观、动力、车联技术3个方面，都给人一种焕然一新的感觉。上汽通用旗下别克品牌的紧凑型轿车，有威朗、英朗与凯越，由此划分出高、中、低三个档次，价格从18万元下探到8万元。类似做法，在德系车阵营当中，同样存在。比如，上汽大众旗下有凌度、朗逸和桑塔纳，一汽-大众旗下有速腾、宝来和捷达，都是高、中、低三档。","video":"/factory_uploads/2019/10/25/20191025112123128.mp4","icon":"/factory_uploads/2019/10/25/20191025112123126.jpg","status":3},{"id":31,"informationName":"绝不仅仅是油改电 试驾广汽本田纯电动车型VE-1","time":1572075000,"words":"随着国内新能源发展的步伐越走越快，越来越多的企业，加入了中国新能源汽车发展的大军。特别是到9102年，笔者注意到，一直态度表现暧昧的合资车企，也开始布局自身的新能源产品。日系企业，在新能源技术研究与应用方面，一直走在世界的前列。而作为其中对于技术和驾驶乐趣的\u201c偏执狂\u201d日本本田汽车，也将与近期在华推出了首款兼具驾驶乐趣的新能源汽车-广汽本田VE -1。笔者有幸在这款车型，上市之前短暂的体验了一下。那么这款车型是否很好的展现了运动与新能源汽车的和谐并存呢？","video":"/factory_uploads/2019/10/25/20191025112249595.mp4","icon":"/factory_uploads/2019/10/25/20191025112249640.jpg","status":3}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 20
         * informationName : 19款丰田霸道2700特惠原装进口环保公开
         * time : 1571972940
         * words : 19款丰田霸道2700是丰田旗下的一款大型SUV，丰田霸道凭仗其良好的越野机能和优良的外观计划和其超高的性价比为其在竞争如斯剧烈的越野车市场夺得冠军，车型如同名字一样，非常的霸道。丰田霸道2700中东版纯正越野利器换了一副全新的面孔重新示人。位于保险杠两侧的圆形雾灯更加的简洁、直观，拥有那仿佛钢铁侠一样的夺人的眼神。全身硬朗的棱线设计更是振奋人心，体现对完美的追求。
         * video : /factory_uploads/2019/10/25/20191025110933256.mp4
         * icon : /factory_uploads/2019/10/25/20191025110933363.jpg
         * status : 0
         */

        private int id;
        private String informationName;
        private int time;
        private String words;
        private String video;
        private String icon;
        private int status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getInformationName() {
            return informationName;
        }

        public void setInformationName(String informationName) {
            this.informationName = informationName;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public String getWords() {
            return words;
        }

        public void setWords(String words) {
            this.words = words;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
