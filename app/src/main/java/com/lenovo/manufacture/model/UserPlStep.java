package com.lenovo.manufacture.model;

import java.util.List;

public class UserPlStep {

    /**
     * status : 200
     * message : SUCCESS
     * data : [{"id":20148,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":-1},{"id":20147,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":24},{"id":20146,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":23},{"id":20145,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":22},{"id":20144,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":21},{"id":20143,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":20},{"id":20142,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":19},{"id":20141,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":18},{"id":20140,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":17},{"id":20139,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":16},{"id":20138,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":15},{"id":20137,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":14},{"id":20136,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":13},{"id":20135,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":12},{"id":20134,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":11},{"id":20133,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":10},{"id":20132,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":9},{"id":20131,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":8},{"id":20130,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":7},{"id":20129,"userWorkId":1,"userProductionLineId":2433,"nextUserPlStepId":6},{"id":20128,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":-1},{"id":20127,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":44},{"id":20126,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":43},{"id":20125,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":42},{"id":20124,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":41},{"id":20123,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":40},{"id":20122,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":39},{"id":20121,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":38},{"id":20120,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":37},{"id":20119,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":36},{"id":20118,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":35},{"id":20117,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":34},{"id":20116,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":33},{"id":20115,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":32},{"id":20114,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":31},{"id":20113,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":30},{"id":20112,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":29},{"id":20111,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":28},{"id":20110,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":27},{"id":20109,"userWorkId":1,"userProductionLineId":2432,"nextUserPlStepId":26}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 20148
         * userWorkId : 1
         * userProductionLineId : 2433
         * nextUserPlStepId : -1
         */

        private int id;
        private int userWorkId;
        private int userProductionLineId;
        private int nextUserPlStepId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserWorkId() {
            return userWorkId;
        }

        public void setUserWorkId(int userWorkId) {
            this.userWorkId = userWorkId;
        }

        public int getUserProductionLineId() {
            return userProductionLineId;
        }

        public void setUserProductionLineId(int userProductionLineId) {
            this.userProductionLineId = userProductionLineId;
        }

        public int getNextUserPlStepId() {
            return nextUserPlStepId;
        }

        public void setNextUserPlStepId(int nextUserPlStepId) {
            this.nextUserPlStepId = nextUserPlStepId;
        }
    }
}
