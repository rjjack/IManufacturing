package com.lenovo.manufacture;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.beans.UserWorkEnvironmentInfo;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TwoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "TwoActivity";
    private TextView tv_temp;
    private ImageView img_cold;
    private ImageView img_worm;
    private Button btn_add;
    private Button btn_jian;
    private int myTemp;
    private int workshopTemp;
    private int outTemp;
    private int cloud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        loadData();
    }

    private void loadData() {
        new Thread() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/UserWorkEnvironmental/getInfo?id=1")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string = response.body().string();
                    Log.d(TAG, "run: loadData" + string);
                    Gson gson = new Gson();
                    UserWorkEnvironmentInfo userWorkEnvironmentInfo = gson.fromJson(string, UserWorkEnvironmentInfo.class);
                    UserWorkEnvironmentInfo.DataBean dataBean = userWorkEnvironmentInfo.getData().get(0);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initView(dataBean);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    private void initView(UserWorkEnvironmentInfo.DataBean dataBean) {
        tv_temp = (TextView) findViewById(R.id.tv_temp);
        tv_temp.setOnClickListener(this);
        img_cold = (ImageView) findViewById(R.id.img_cold);
        img_cold.setOnClickListener(this);
        img_worm = (ImageView) findViewById(R.id.img_worm);
        img_worm.setOnClickListener(this);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_jian = (Button) findViewById(R.id.btn_jian);
        btn_jian.setOnClickListener(this);

        workshopTemp = Integer.parseInt(dataBean.getWorkshopTemp().replace("℃", ""));
        outTemp = Integer.parseInt(dataBean.getOutTemp().replace("℃", ""));
        tv_temp.setText(workshopTemp + "℃");
        myTemp = Integer.parseInt(tv_temp.getText().toString().trim().replace("℃", ""));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                myTemp++;
                tv_temp.setText(myTemp + "℃");
                checkTemp();
                break;
            case R.id.btn_jian:
                myTemp--;
                tv_temp.setText(myTemp + "℃");
                checkTemp();
                break;
        }
    }

    private void checkTemp() {
        if (workshopTemp >= myTemp) {
            //开启冷风
            if (cloud!=1) {
                img_cold.setImageDrawable(getDrawable(R.drawable.cold_0001));
                img_worm.setImageDrawable(getDrawable(R.drawable.warm_0001));
                cloud = 1;
                Toast.makeText(getApplicationContext(), "开启冷风", Toast.LENGTH_SHORT).show();
            }
        } else {
            //开启热风
            if (cloud!=2) {
                img_cold.setImageDrawable(getDrawable(R.drawable.cold_0002));
                img_worm.setImageDrawable(getDrawable(R.drawable.warm_0002));
                cloud = 2;
                Toast.makeText(getApplicationContext(), "开启热风", Toast.LENGTH_SHORT).show();
            }
        }
        new Thread() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/UserWorkEnvironmental/updateAcOnOff?id=1&acOnOff=" + cloud)
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }
}