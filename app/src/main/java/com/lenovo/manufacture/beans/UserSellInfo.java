package com.lenovo.manufacture.beans;

import java.util.List;

public class UserSellInfo {
    /**
     * status : 200
     * message : SUCCESS
     * data : [{"id":17489,"userFactoryId":1,"carTypeId":2,"price":800,"time":1576135879,"num":5,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17488,"userFactoryId":1,"carTypeId":3,"price":500,"time":1574452584,"num":10,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17487,"userFactoryId":1,"carTypeId":2,"price":1567,"time":1570112584,"num":1,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17486,"userFactoryId":1,"carTypeId":1,"price":2666,"time":1570236932,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17485,"userFactoryId":1,"carTypeId":2,"price":1600,"time":1576703568,"num":1,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17484,"userFactoryId":1,"carTypeId":1,"price":1500,"time":1575215879,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17483,"userFactoryId":1,"carTypeId":2,"price":896,"time":1571211111,"num":5,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17482,"userFactoryId":1,"carTypeId":2,"price":1500,"time":1570101258,"num":3,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17481,"userFactoryId":1,"carTypeId":3,"price":2000,"time":1570610147,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17480,"userFactoryId":1,"carTypeId":1,"price":1396,"time":1570512547,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17479,"userFactoryId":1,"carTypeId":2,"price":136,"time":1570332589,"num":20,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17478,"userFactoryId":1,"carTypeId":3,"price":1555,"time":1576425879,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17477,"userFactoryId":1,"carTypeId":1,"price":1000,"time":1574512584,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17476,"userFactoryId":1,"carTypeId":2,"price":1002,"time":1570512584,"num":5,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17475,"userFactoryId":1,"carTypeId":1,"price":259,"time":1570356932,"num":10,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17474,"userFactoryId":1,"carTypeId":2,"price":920,"time":1576796742,"num":5,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17473,"userFactoryId":1,"carTypeId":1,"price":100,"time":1575237888,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17472,"userFactoryId":1,"carTypeId":1,"price":363,"time":1571273888,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17471,"userFactoryId":1,"carTypeId":3,"price":588,"time":1570136120,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17470,"userFactoryId":1,"carTypeId":3,"price":1800,"time":1570635120,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17469,"userFactoryId":1,"carTypeId":1,"price":1533,"time":1570535840,"num":2,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17468,"userFactoryId":1,"carTypeId":3,"price":1266,"time":1570343085,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17467,"userFactoryId":1,"carTypeId":3,"price":1525,"time":1576452058,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17466,"userFactoryId":1,"carTypeId":3,"price":2000,"time":1574534059,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17465,"userFactoryId":1,"carTypeId":2,"price":1852,"time":1570578909,"num":2,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17464,"userFactoryId":1,"carTypeId":3,"price":2229,"time":1570356239,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17463,"userFactoryId":1,"carTypeId":2,"price":960,"time":1576769321,"num":15,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17462,"userFactoryId":1,"carTypeId":1,"price":456,"time":1575233214,"num":7,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17461,"userFactoryId":1,"carTypeId":1,"price":3600,"time":1571278234,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17460,"userFactoryId":1,"carTypeId":3,"price":5858,"time":1570159678,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17459,"userFactoryId":1,"carTypeId":3,"price":800,"time":1570646989,"num":3,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17458,"userFactoryId":1,"carTypeId":1,"price":133,"time":1570545839,"num":25,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17457,"userFactoryId":1,"carTypeId":3,"price":1266,"time":1570345639,"num":6,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17456,"userFactoryId":1,"carTypeId":1,"price":1225,"time":1576456639,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17455,"userFactoryId":1,"carTypeId":3,"price":200,"time":1574535569,"num":20,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17454,"userFactoryId":1,"carTypeId":2,"price":1562,"time":1570557339,"num":2,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17453,"userFactoryId":1,"carTypeId":1,"price":2569,"time":1570345239,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17452,"userFactoryId":1,"carTypeId":1,"price":293,"time":1576789321,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17451,"userFactoryId":1,"carTypeId":2,"price":258,"time":1574133214,"num":7,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17450,"userFactoryId":1,"carTypeId":1,"price":205,"time":1571231234,"num":13,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17449,"userFactoryId":1,"carTypeId":1,"price":725,"time":1570125678,"num":2,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17448,"userFactoryId":1,"carTypeId":3,"price":1200,"time":1570646789,"num":15,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17447,"userFactoryId":1,"carTypeId":1,"price":158,"time":1570553839,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17446,"userFactoryId":1,"carTypeId":3,"price":1266,"time":1570343239,"num":6,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17445,"userFactoryId":1,"carTypeId":3,"price":1205,"time":1576443639,"num":10,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17444,"userFactoryId":1,"carTypeId":2,"price":200,"time":1574552569,"num":10,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17443,"userFactoryId":1,"carTypeId":1,"price":152,"time":1570567339,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17442,"userFactoryId":1,"carTypeId":1,"price":236,"time":1570355239,"num":10,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17441,"userFactoryId":1,"carTypeId":1,"price":666,"time":1570456321,"num":3,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17440,"userFactoryId":1,"carTypeId":2,"price":288,"time":1570223214,"num":7,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17439,"userFactoryId":1,"carTypeId":1,"price":200,"time":1570321234,"num":3,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17438,"userFactoryId":1,"carTypeId":3,"price":720,"time":1570235678,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17437,"userFactoryId":1,"carTypeId":3,"price":1080,"time":1570356789,"num":6,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17436,"userFactoryId":1,"carTypeId":3,"price":1808,"time":1570567839,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17435,"userFactoryId":1,"carTypeId":2,"price":1262,"time":1570321239,"num":4,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17434,"userFactoryId":1,"carTypeId":2,"price":360,"time":1570355639,"num":10,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17433,"userFactoryId":1,"carTypeId":2,"price":7200,"time":1573452569,"num":1,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17432,"userFactoryId":1,"carTypeId":2,"price":1555,"time":1570567339,"num":1,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17431,"userFactoryId":1,"carTypeId":1,"price":2666,"time":1570356239,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17430,"userFactoryId":1,"carTypeId":3,"price":488,"time":1570412439,"num":3,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17429,"userFactoryId":1,"carTypeId":3,"price":355,"time":1570229643,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17428,"userFactoryId":1,"carTypeId":1,"price":1000,"time":1570323239,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17427,"userFactoryId":1,"carTypeId":3,"price":769,"time":1570232979,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17426,"userFactoryId":1,"carTypeId":3,"price":876,"time":1570354589,"num":30,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17425,"userFactoryId":1,"carTypeId":1,"price":1366,"time":1570236839,"num":25,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17424,"userFactoryId":1,"carTypeId":2,"price":1280,"time":1570371239,"num":4,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17423,"userFactoryId":1,"carTypeId":2,"price":3868,"time":1570357839,"num":20,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17422,"userFactoryId":1,"carTypeId":1,"price":780,"time":1570362569,"num":13,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17421,"userFactoryId":1,"carTypeId":1,"price":750,"time":1570324339,"num":6,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17420,"userFactoryId":1,"carTypeId":3,"price":2500,"time":1570352239,"num":8,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17419,"userFactoryId":1,"carTypeId":3,"price":400,"time":1570423439,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17418,"userFactoryId":1,"carTypeId":3,"price":3555,"time":1570226543,"num":3,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17417,"userFactoryId":1,"carTypeId":1,"price":333,"time":1570352239,"num":15,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17416,"userFactoryId":1,"carTypeId":3,"price":221,"time":1570232239,"num":10,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17415,"userFactoryId":1,"carTypeId":3,"price":830,"time":1570354239,"num":3,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17414,"userFactoryId":1,"carTypeId":1,"price":1360,"time":1570232239,"num":7,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17413,"userFactoryId":1,"carTypeId":2,"price":1580,"time":1570372239,"num":5,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17412,"userFactoryId":1,"carTypeId":2,"price":3888,"time":1570352239,"num":6,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17411,"userFactoryId":1,"carTypeId":1,"price":750,"time":1570362339,"num":6,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17410,"userFactoryId":1,"carTypeId":1,"price":700,"time":1570325239,"num":2,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17409,"userFactoryId":1,"carTypeId":3,"price":1500,"time":1570342239,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17408,"userFactoryId":1,"carTypeId":1,"price":600,"time":1570692060,"num":2,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17407,"userFactoryId":1,"carTypeId":3,"price":4000,"time":1570323439,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17406,"userFactoryId":1,"carTypeId":3,"price":3500,"time":1570326543,"num":3,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17405,"userFactoryId":1,"carTypeId":1,"price":300,"time":1570332239,"num":8,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17404,"userFactoryId":1,"carTypeId":3,"price":200,"time":1570372239,"num":1,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17403,"userFactoryId":1,"carTypeId":3,"price":800,"time":1570352239,"num":2,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17402,"userFactoryId":1,"carTypeId":1,"price":1600,"time":1570222239,"num":6,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17401,"userFactoryId":1,"carTypeId":2,"price":1400,"time":1570352239,"num":6,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17400,"userFactoryId":1,"carTypeId":2,"price":3000,"time":1570312239,"num":2,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17399,"userFactoryId":1,"carTypeId":1,"price":700,"time":1570322339,"num":3,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17398,"userFactoryId":1,"carTypeId":1,"price":600,"time":1570325239,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17397,"userFactoryId":1,"carTypeId":3,"price":2500,"time":1570312239,"num":5,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17396,"userFactoryId":1,"carTypeId":3,"price":2500,"time":1570322239,"num":10,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17395,"userFactoryId":1,"carTypeId":1,"price":1500,"time":1570311239,"num":5,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17394,"userFactoryId":1,"carTypeId":2,"price":500,"time":1570611239,"num":2,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17393,"userFactoryId":1,"carTypeId":1,"price":100,"time":1570611239,"num":2,"lastUpdateTime":null,"carTypeName":"轿车汽车"},{"id":17392,"userFactoryId":1,"carTypeId":3,"price":500,"time":1570638239,"num":6,"lastUpdateTime":null,"carTypeName":"SUV汽车"},{"id":17391,"userFactoryId":1,"carTypeId":2,"price":200,"time":1570628239,"num":2,"lastUpdateTime":null,"carTypeName":"MPV汽车"},{"id":17390,"userFactoryId":1,"carTypeId":1,"price":100,"time":1570681239,"num":1,"lastUpdateTime":null,"carTypeName":"轿车汽车"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 17489
         * userFactoryId : 1
         * carTypeId : 2
         * price : 800
         * time : 1576135879
         * num : 5
         * lastUpdateTime : null
         * carTypeName : MPV汽车
         */

        private int id;
        private int userFactoryId;
        private int carTypeId;
        private int price;
        private int time;
        private int num;
        private Object lastUpdateTime;
        private String carTypeName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserFactoryId() {
            return userFactoryId;
        }

        public void setUserFactoryId(int userFactoryId) {
            this.userFactoryId = userFactoryId;
        }

        public int getCarTypeId() {
            return carTypeId;
        }

        public void setCarTypeId(int carTypeId) {
            this.carTypeId = carTypeId;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public Object getLastUpdateTime() {
            return lastUpdateTime;
        }

        public void setLastUpdateTime(Object lastUpdateTime) {
            this.lastUpdateTime = lastUpdateTime;
        }

        public String getCarTypeName() {
            return carTypeName;
        }

        public void setCarTypeName(String carTypeName) {
            this.carTypeName = carTypeName;
        }
    }
}
