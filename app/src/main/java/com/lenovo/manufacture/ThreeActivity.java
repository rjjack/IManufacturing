package com.lenovo.manufacture;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.lenovo.manufacture.beans.AllPeople;
import com.lenovo.manufacture.beans.UserSellInfo;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ThreeActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView5;
    private TextView textView6;
    private ListView lv_three;
    private MyAdapter myAdapter;
    private boolean typeFlag = false;
    private boolean moneyFlag = false;
    private List<AllPeople.DataBean> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);

        initView();

        loadData();

    }

    private void initView() {

        textView5 = (TextView) findViewById(R.id.textView5);
        textView5.setOnClickListener(this);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView6.setOnClickListener(this);
        lv_three = (ListView) findViewById(R.id.lv_three);
    }

    private void loadData() {
        new Thread() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://192.168.1.103:8085/dataInterface/People/getAll")
                        .method("GET", null)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string = response.body().string();
                    Gson gson = new Gson();
                    AllPeople allPeople = gson.fromJson(string, AllPeople.class);
                    data = allPeople.getData();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initList(data);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }.start();
    }

    private void initList(List<AllPeople.DataBean> data) {
        myAdapter = new MyAdapter(data);
        lv_three.setAdapter(myAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView5:
                if (typeFlag) {
                    Collections.sort(data, new Comparator<AllPeople.DataBean>() {
                        @Override
                        public int compare(AllPeople.DataBean o1, AllPeople.DataBean o2) {
                            return o1.getStatus() - o2.getStatus();
                        }
                    });
                    textView5.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0002), null);
                } else {
                    Collections.sort(data, new Comparator<AllPeople.DataBean>() {
                        @Override
                        public int compare(AllPeople.DataBean o1, AllPeople.DataBean o2) {
                            return o2.getStatus() - o1.getStatus();
                        }
                    });
                    textView5.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0004), null);
                }
                typeFlag = !typeFlag;
                myAdapter.notifyDataSetChanged();
                break;
            case R.id.textView6:
                if (moneyFlag) {
                    Collections.sort(data, new Comparator<AllPeople.DataBean>() {
                        @Override
                        public int compare(AllPeople.DataBean o1, AllPeople.DataBean o2) {
                            return o1.getGold() - o2.getGold();
                        }
                    });
                    textView6.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0002), null);
                } else {
                    Collections.sort(data, new Comparator<AllPeople.DataBean>() {
                        @Override
                        public int compare(AllPeople.DataBean o1, AllPeople.DataBean o2) {
                            return o2.getGold() - o1.getGold();
                        }
                    });
                    textView6.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.sanjao0004), null);
                }
                moneyFlag = !moneyFlag;
                myAdapter.notifyDataSetChanged();
                break;
        }
    }

    class MyAdapter extends BaseAdapter {
        List<AllPeople.DataBean> data;

        public MyAdapter(List<AllPeople.DataBean> data) {
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = View.inflate(getApplicationContext(), R.layout.item_three_lv, null);
            } else {
                view = convertView;
            }
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.tv_name.setText(data.get(position).getPeopleName());
            viewHolder.tv_money.setText(data.get(position).getGold() + "");
            //(0、工程师，1、工人，2、技术人员，3、检测人员)
            String type;
            switch (data.get(position).getStatus()) {
                case 0:
                    type = "工程师";
                    break;
                case 1:
                    type = "工人";
                    break;
                case 2:
                    type = "技术人员";
                    break;
                case 3:
                    type = "检测人员";
                    break;
                default:
                    type = "未知";
                    break;
            }

            viewHolder.tv_type.setText(type);

            return view;
        }

        class ViewHolder {
            public View rootView;
            public TextView tv_name;
            public TextView tv_type;
            public TextView tv_money;

            public ViewHolder(View rootView) {
                this.rootView = rootView;
                this.tv_name = (TextView) rootView.findViewById(R.id.tv_name);
                this.tv_type = (TextView) rootView.findViewById(R.id.tv_type);
                this.tv_money = (TextView) rootView.findViewById(R.id.tv_money);
            }

        }
    }
}